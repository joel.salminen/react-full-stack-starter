import express from 'express';
import debugModule from 'debug';
import morgan from 'morgan';
import { cyan } from 'chalk';
import { port } from './config.js';
import serverRender from './renderers/server.js';
import data from './data.json';

const debug = debugModule('app');
const app = express();

app.use(morgan('tiny'));
app.use(express.static('public'));
app.set('view engine', 'ejs');

app.get('/', async (req, res) => {
  const initialContent = await serverRender();
  res.render('index', { ...initialContent });
});

app.get('/data', (req, res) => {
  res.send({ cards: data });
});

app.listen(port, () => {
  console.clear();
  console.log(cyan('Server Started')); // eslint-disable-line no-console
  debug(`Listening on port ${cyan(port)}`);
});
