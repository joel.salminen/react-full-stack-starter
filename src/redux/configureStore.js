import { createStore, applyMiddleware, compose } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import rootReducer from './reducers/rootReducer.js';
import thunk from 'redux-thunk';

const configureStore = initialState => {
  if (global.window) {
    const composeEnhancers =
      global.window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    return createStore(
      rootReducer,
      initialState,
      composeEnhancers(applyMiddleware(thunk, reduxImmutableStateInvariant()))
    );
  }
  return createStore(rootReducer, initialState);
};

export default configureStore;
