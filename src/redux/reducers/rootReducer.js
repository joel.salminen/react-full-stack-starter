import { combineReducers } from 'redux';
import cards from './cardReducer.js';

const rootReducer = combineReducers({
  cards
});

export default rootReducer;
