import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from 'react-redux';
import App from '../components/App';
import configureStore from '../redux/configureStore.js';

const store = configureStore(window.initialData);

ReactDOM.hydrate(
  <ReduxProvider store={store}>
    <App />
  </ReduxProvider>,
  document.getElementById('root')
);
