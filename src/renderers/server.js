import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Provider as ReduxProvider } from 'react-redux';
import axios from 'axios';
import { host, port } from '../config.js';
import configureStore from '../redux/configureStore.js';
import App from '../components/App';

const serverRender = async () => {
  const url = `http://${host}:${port}/data`;
  const resp = await axios.get(url);
  const store = configureStore(resp.data);
  return {
    initialMarkup: ReactDOMServer.renderToString(
      <ReduxProvider store={store}>
        <App />
      </ReduxProvider>
    ),
    initialData: resp.data
  };
};

export default serverRender;
