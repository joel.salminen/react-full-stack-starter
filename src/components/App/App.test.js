import React from 'react';
import App from './App.js';
import { shallow } from 'enzyme';

describe('App', () => {
  it('should render', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('div')).toHaveLength(1);
  });
});
