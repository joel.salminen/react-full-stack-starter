import React from 'react';
import { connect } from 'react-redux';

const App = ({ cards }) => {
  return (
    <div>
      { cards.map(card => <div key={card.dbfId}>{card.name}</div>) }
    </div>
  );
};

const mapStateToProps = ({ cards }) => {
  return {
    cards
  };
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
